<?php  
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Valider fiche de frais"
 * @package default
 * @todo  RAS
 */
$repInclude = './include/';
require($repInclude . "_init.inc.php");

// page inaccessible si comptable non connecté
if (!estComptableConnecte()) 
{
	header("Location: cSeConnecter.php");
	die();
}

//configuration des éléments nécéssaires au fonctionnement de la page
$etape = lireDonneePost("etape","demandeRecherche");
$titre = "Selectionner la lettre pour lancer une recherche visiteur";
$nomVisiteur = lireDonneePost("txtNom","");
$prenomVisiteur = lireDonneePost("txtPrenom","");
$idVisiteur = lireDonneePost("txtID","");
require($repInclude . "_entete.inc.html");
require($repInclude . "_sommaire.inc.php");

//Traite les données récupérés par la methode GET du menu recherche
$lettre = strtoupper(substr(lireDonnee("lettre","A"),0,1));
//Verifie que la lettre est bien un charactère entre A et Z
$lettre = (preg_match('/^[A-Z]$/',$lettre))?$lettre:" ";
$titre = "Voici la liste des visiteurs pour la lettre ".$lettre;

//Configuration de la pagination
$pageCourante = lireDonnee("page","");
$nbEntre = count(obtenirAlphabetVisiteurs($idConnexion, $lettre));
$nbEntreParPages = 5;
$nbPages = ceil($nbEntre/$nbEntreParPages);
if (!(is_numeric($pageCourante) && $pageCourante<=$nbPages))
{
	$pageCourante = 1;
}
$limiteMin = ($pageCourante-1)*$nbEntreParPages;
//Récupération des données selon la pagination
$visiteurs = obtenirAlphabetVisiteurs($idConnexion, $lettre,$limiteMin,$nbEntreParPages);
//Erreur si aucune liste de visiteurs trouvée
if(!$visiteurs && $lettre !=" ")
{
	ajouterErreur($tabErreurs, "Pas de visiteur pour cette lettre ");
}

// structure de décision sur les différentes étapes du cas d'utilisation
if ($etape=='validerRecherche') 
{ 
	// un comptable demande à afficher les fiches de frais concernant un visiteur
	// si l'id du visiteur a été trouvé, donc informations fournies sous forme de tableau
	$fichesFrais = obtenirFichesFraisVisiteur($idConnexion,$idVisiteur);
	//si les données sont biens récupérés on passe à l'étapes suivante
	if (is_array($fichesFrais)) 
	{
		$etape ='afficheRecherche';
	}
	else
	{
		ajouterErreur($tabErreurs, "Pas de fiches de frais pour le visiteur : $nomVisiteur $prenomVisiteur");
		$etape ='demandeRecherche';
	}
}

//Parcourt le tableau $fichesFrais et enregistre tout les mois 
if ($etape =='afficheRecherche')
{
	foreach ($fichesFrais as $uneFiche) 
	{
		$mois[] = $uneFiche['mois'];
	}
	$titre = "Veuillez séléctioner le mois pour le visiteur : $nomVisiteur $prenomVisiteur";
}

//affiche la fiche frais selon le mois séléctionné
if ($etape =='finaliserRecherche') 
{
	$moisSaisi = lireDonneePost("lstMois","");
	$req = obtenirReqEltsForfaitFicheFrais($moisSaisi,$idVisiteur);
	$tabFicheFrais = obtenirDetailFicheFrais($idConnexion, $moisSaisi, $idVisiteur);
	$titre = "Voici la fiche de frais pour le visiteur : $nomVisiteur $prenomVisiteur";
}
?>

<!-- Division principale -->
<div id="contenu">
	<h2>Chercher un visiteur</h2>
	<?php
	if ( $etape == "validerRecherche" || $etape =="afficheRecherche" || isset($lettre)) 
	{
		if ( nbErreurs($tabErreurs) > 0 ) 
		{
			echo toStringErreurs($tabErreurs);
		}
	}
	?>
		<h3><?php echo $titre ?></h3>

		<?php if($etape =='demandeRecherche'): ?>
		<!-- Afichage 1 ère étape -->
		<div class="corpsForm">
			<?php require($repInclude . "_menuAlphabetique.inc.php") ?>
		</div>
		<?php endif; ?>

		<?php if(isset($visiteurs) && is_array($visiteurs) && $etape =="demandeRecherche"):?>
		<!-- Afichage 2 ème étape -->
		<div class="corpsForm">
			<div>
			<span> Total trouvé :<?php echo $nbEntre; ?> visiteur(s)</span>
			<table class="listeLegere" style="table-layout: fixed;width:100%;">
			<?php foreach ($visiteurs as $visiteur): ?>
				<tr>
					<th width="10%">ID</th>
					<th width="15%">Nom</th>
					<th width="15%">Prenom</th>
					<th width="30%">Adresse</th>
					<th class="action" width="30%"> &nbsp; </th>
				</tr>
				<tr>
					<td>
						<?php echo $visiteur['id'] ?>
					</td>
					<td>
						<?php echo $visiteur['nom'] ?>
					</td>
					<td>
						<?php echo $visiteur['prenom'] ?>
					</td>
					<td>
						<?php echo $visiteur['adresse'] ?>
					</td>
					<td>
						<form action="" method="post">
							<input type="hidden" name="txtID" value="<?php echo $visiteur['id'] ?>"/>
							<input type="hidden" name="txtNom" value="<?php echo $visiteur['nom'] ?>"/>
							<input type="hidden" name="txtPrenom" value="<?php echo $visiteur['prenom'] ?>"/>
							<input type="hidden" name="etape" value="validerRecherche"/>
							<input type="submit" value="Consulter fiche de frais" style="width:100%; word-wrap:break-word">
						</form>
					</td>
				</tr>
			<?php endforeach ?>
			</table>
		</div>
		<?php require($repInclude . "_pagination.inc.php") ?>
	</div>

		<?php elseif ($etape =='afficheRecherche'):?>
		<!-- Afichage 3 ème étape -->
		<form action="" method="post">
		<div class="corpsForm">
		<input type="hidden" name="etape" value="finaliserRecherche"/>
		<input type="hidden" name="txtID" value="<?php echo $idVisiteur ?>"/>
		<input type="hidden" name="txtNom" value="<?php echo $nomVisiteur ?>"/>
		<input type="hidden" name="txtPrenom" value="<?php echo $prenomVisiteur ?>"/>
		<label for="lstMois">Mois : </label>
		<select id="lstMois" name="lstMois" title="Sélectionnez le mois souhaité pour la fiche de frais">
			<?php foreach($mois as $unMois): 
			$noMois = intval(substr($unMois, 4, 2));
			$annee = intval(substr($unMois, 0, 4));
			?>
			<option value="<?php echo $unMois ?>"><?php echo obtenirLibelleMois($noMois) .' '.$annee ?></option>
		<?php endforeach; ?>
		</select>
		</div>
		<div class="piedForm">
			<p>
				<input id="ok" type="submit" value="Valider" size="20"
				title="Demandez à suivre cette fiche de frais" />
				<input id="annuler" type="reset" value="Effacer" size="20" />
			</p> 
		</div>
	</form>
	
<?php elseif($etape=='finaliserRecherche'): 
require($repInclude."_tableauFicheFrais.inc.php"); ?>
<div class="piedForm">
	<p>
		<input type="button" value="Retour" size="20" 
		title="Retourner à la page précédente" onclick="history.back();" />
	</p> 
</div>
<?php endif ?>
</div>
<?php
require($repInclude . "_pied.inc.html");
require($repInclude . "_fin.inc.php");
?>