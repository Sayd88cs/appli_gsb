<?php
/** 
 * Initialise les ressources nécessaires au fonctionnement de l'application
 * @package default
 * @todo  RAS
 */
  require("_bdGestionDonnees.lib.php");
  require("_gestionSession.lib.php");
  require("_utilitairesEtGestionErreurs.lib.php");
  setlocale (LC_TIME, 'fr_FR','fra'); 
  define('WEBROOT',dirname($_SERVER['SCRIPT_NAME']));
  // démarrage ou reprise de la session
  initSession();
  // initialement, aucune erreur
  $tabErreurs = array();
    
  // Demande-t-on une déconnexion ?
  $demandeDeconnexion = lireDonneeUrl("cmdDeconnecter");
  if ( $demandeDeconnexion == "on") {
      deconnecterVisiteur();
      header("Location: cAccueil.php");
      die();
  }
  // établissement d'une connexion avec le serveur de données 
  // puis sélection de la BD qui contient les données des visiteurs et de leurs frais
  $idConnexion=connecterServeurBD();
  if (!$idConnexion) {
      ajouterErreur($tabErreurs, "Echec de la connexion au serveur MySql");
  }
  elseif (!activerBD($idConnexion)) {
      ajouterErreur($tabErreurs, "La base de données de l'application est inexistante ou non accessible");
  }
?>