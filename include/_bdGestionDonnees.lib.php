<?php
/** 
 * Regroupe les fonctions d'acc�s aux donn�es.
 * @package default
 * @author Arthur Martin
 * @todo Fonctions retournant plusieurs lignes sont � r��crire.
 */

/** 
 * Se connecte au serveur de donn�es MySql.
 * Se connecte au serveur de donn�es MySql � partir de valeurs
 * pr�d�finies de connexion (h�te, compte utilisateur et mot de passe). 
 * Retourne l'identifiant de connexion si succ�s obtenu, le bool�en false 
 * si probl�me de connexion.
 * @return resource identifiant de connexion
 */
function connecterServeurBD() {
    $hote = "localhost";
    $login = "root";
    $mdp = "root";
    return mysql_connect($hote, $login, $mdp);
}

/**
 * S�lectionne (rend active) la base de donn�es.
 * S�lectionne (rend active) la BD pr�d�finie gsb_frais sur la connexion
 * identifi�e par $idCnx. Retourne true si succ�s, false sinon.
 * @param resource $idCnx identifiant de connexion
 * @return boolean succ�s ou �chec de s�lection BD 
 */
function activerBD($idCnx) {
    $bd = "gsbv2";
    $query = "SET CHARACTER SET utf8";
    // Modification du jeu de caract�res de la connexion
    $res = mysql_query($query, $idCnx); 
    $ok = mysql_select_db($bd, $idCnx);
    return $ok;
}

/** 
 * Ferme la connexion au serveur de donn�es.
 * Ferme la connexion au serveur de donn�es identifi�e par l'identifiant de 
 * connexion $idCnx.
 * @param resource $idCnx identifiant de connexion
 * @return void  
 */
function deconnecterServeurBD($idCnx) {
    mysql_close($idCnx);
}

/**
 * Echappe les caract�res sp�ciaux d'une cha�ne.
 * Envoie la cha�ne $str �chapp�e, c�d avec les caract�res consid�r�s sp�ciaux
 * par MySql (tq la quote simple) pr�c�d�s d'un \, ce qui annule leur effet sp�cial
 * @param string $str cha�ne � �chapper
 * @return string cha�ne �chapp�e 
 */    
function filtrerChainePourBD($str) {
    if ( ! get_magic_quotes_gpc() ) { 
        // si la directive de configuration magic_quotes_gpc est activ�e dans php.ini,
        // toute cha�ne re�ue par get, post ou cookie est d�j� �chapp�e 
        // par cons�quent, il ne faut pas �chapper la cha�ne une seconde fois
        $str = mysql_real_escape_string($str);
    }
    return $str;
}

/** 
 * Fournit les informations sur un visiteur demand�. 
 * Retourne les informations du visiteur d'id $unId sous la forme d'un tableau
 * associatif dont les cl�s sont les noms des colonnes(id, nom, prenom).
 * @param resource $idCnx identifiant de connexion
 * @param string $unId id de l'utilisateur
 * @return array  tableau associatif du visiteur
 */
function obtenirDetailEmploye($idCnx, $unId) {
    $id = filtrerChainePourBD($unId);
    $requete = "SELECT id, nom, prenom FROM employe WHERE id ='" . $unId . "'";
    $idJeuRes = mysql_query($requete, $idCnx);  
    $ligne = false;     
    if ( $idJeuRes ) {
        $ligne = mysql_fetch_assoc($idJeuRes);
        mysql_free_result($idJeuRes);
    }
    return $ligne ;
}

/** 
 * Fournit les informations sur un visiteur demand�. 
 * Retourne les informations du visiteur d'id $unId sous la forme d'un tableau
 * associatif dont les cl�s sont les noms des colonnes.
 * @param resource $idCnx identifiant de connexion
 * @param string $unId id de l'utilisateur
 * @return array  tableau associatif du visiteur
 */
function obtenirTousDetailEmploye($idCnx, $unId) {
    $id = filtrerChainePourBD($unId);
    $requete = "SELECT nom,prenom AS 'pr�nom',adresse,ville,cp AS 'code Postal' FROM employe WHERE id='" . $unId . "'";
    $idJeuRes = mysql_query($requete, $idCnx);  
    $ligne = false;
    if ( $idJeuRes ) {
        $ligne = mysql_fetch_assoc($idJeuRes);
        mysql_free_result($idJeuRes);
    }
    return $ligne ;
}

/** 
 * Fournit les informations sur un visiteur demand�. 
 * Retourne les informations du visiteur d'id $unId sous la forme d'un tableau
 * associatif dont les cl�s sont les noms des colonnes.
 * @param resource $idCnx identifiant de connexion
 * @param string $unId id de l'utilisateur
 * @return array  tableau associatif des visiteurs
 */
function obtenirTousLesVisiteurs($idCnx) {
    $requete = "SELECT id, CONCAT(nom,' ',prenom) AS nomPrenom FROM employe,visiteur WHERE id= idEmploye";
    $idJeuRes = mysql_query($requete, $idCnx);
    $lignes = false;
    while($ligne = mysql_fetch_assoc($idJeuRes)) {
        $lignes[] = $ligne;
    }
    mysql_free_result($idJeuRes);
    return $lignes;
}



/** 
 * Fournit les informations sur les visiteurs demand�s
 * Retourne les informations des visiteurs selon la premi�re lettre du nom
 * sous forme d'un tableau associatif qui � chaques cl�s associe les collones r�cup�r�s.
 * @param resource $idCnx identifiant de connexion, $lettre pour filtrer
 * @return array  tableau associatif des visiteurs
 */
function obtenirAlphabetVisiteurs($idCnx,$lettre,$limitMin=null,$limitMax=null){
    $lettre = filtrerChainePourBD($lettre);
    //Lorsqu'une pagination est d�finie
    if($limitMax && $limitMax)
    {
        $req = "SELECT id,nom,prenom,CONCAT(adresse,',',ville,',',cp) as adresse FROM employe,visiteur WHERE LEFT(nom,1) = '$lettre' AND id IN (idEmploye) LIMIT $limitMin,$limitMax";
    }
    else
    {
        $req ="SELECT id as adresse FROM employe,visiteur WHERE LEFT(nom,1) = '$lettre' AND id IN (idEmploye)";
    }
    $idJeuRes = mysql_query($req, $idCnx);  
    $lignes = false;
    while($ligne = mysql_fetch_assoc($idJeuRes)) {
        $lignes[] = $ligne;
    }
    mysql_free_result($idJeuRes);
    return $lignes;
}

/** 
 * Fournit les informations d'une fiche de frais. 
 * Retourne les informations de la fiche de frais du mois de $unMois (MMAAAA)
 * sous la forme d'un tableau associatif dont les cl�s sont les noms des colonnes
 * (nbJustitificatifs, idEtat, libelleEtat, dateModif, montantValide).
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demand� (MMAAAA)
 * @param string $unIdVisiteur id visiteur  
 * @param string $unEtat �tat de la fiche frais, optionnel
 * @return array tableau associatif de la fiche de frais
 */
function obtenirDetailFicheFrais($idCnx, $unMois, $unIdVisiteur,$unEtat=null) {
    $unMois = filtrerChainePourBD($unMois);
    $ligne = false;
    $requete="select IFNULL(nbJustificatifs,0) as nbJustificatifs, Etat.id as idEtat, libelle as libelleEtat, dateModif, montantValide 
    from FicheFrais inner join Etat on idEtat = Etat.id 
    where idVisiteur='" . $unIdVisiteur . "' and mois='" . $unMois . "'";
    $requete.=($unEtat)?" AND Etat.id LIKE '".$unEtat."'":"";
    $idJeuRes = mysql_query($requete, $idCnx);
    if ( $idJeuRes ) {
        $ligne = mysql_fetch_assoc($idJeuRes);
    }
    mysql_free_result($idJeuRes);
    
    return $ligne ;
}
              
/** 
 * V�rifie si une fiche de frais existe ou non. 
 * Retourne true si la fiche de frais du mois de $unMois (MMAAAA) du visiteur 
 * $idVisiteur existe, false sinon. 
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demand� (MMAAAA)
 * @param string $unIdVisiteur id visiteur 
 * @param string $idEtat optionnel 
 * @return bool�en existence ou non de la fiche de frais
 */
function existeFicheFrais($idCnx, $unMois, $unIdVisiteur,$idEtat=null) {
    $unMois = filtrerChainePourBD($unMois);
    $req = "select idVisiteur from FicheFrais where idVisiteur='$unIdVisiteur' and mois='$unMois'";
    $req .= ($idEtat)?"AND idEtat='$idEtat'":"";
    $idJeuRes = mysql_query($req, $idCnx);
    $ligne = false ;
    if ( $idJeuRes ) 
    {
        $ligne = mysql_fetch_assoc($idJeuRes);
        mysql_free_result($idJeuRes);
    }        
    
    // si $ligne est un tableau, la fiche de frais existe, sinon elle n'exsite pas
    return is_array($ligne) ;
}

/** 
 * V�rifie l'existance d'au moins une fiche frais pour un visiteur. 
 * Retourne les occurences trouv�es si le visiteur � au moins une fiche frais. 
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdVisiteur id visiteur  
 * @return un tableau contenant la/les lignes retourn�es
 */
function obtenirFichesFraisVisiteur($idCnx,$unIdVisiteur,$mois=null,$idEtat=null) {
    $unIdVisiteur = filtrerChainePourBD($unIdVisiteur);
    $req = "SELECT * FROM FicheFrais WHERE idVisiteur = '$unIdVisiteur'";
    $req.=($mois)?" AND mois = '".filtrerChainePourBD($mois)."'":"";
    $req.=($idEtat)?" AND idEtat = '".filtrerChainePourBD($idEtat)."'":"";
    $req.=" ORDER BY mois" ;
    $idJeuRes = mysql_query($req, $idCnx);
    $lignes = false ;
    if ( $idJeuRes ) {
        while ($ligne = mysql_fetch_assoc($idJeuRes)) 
        {
            $lignes[] = $ligne;
     }
        mysql_free_result($idJeuRes);
    }
    return $lignes ;
}

/** 
 * Retourne toutes les fiches de frais trouv�es selon 2 conditions optionelles (le mois et l'�tat). 
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdVisiteur id visiteur 
 * @param string $mois mois fiche frais
 * @param string $idEtat etat fiche frais
 * @return un tableau contenant la/les lignes retourn�es
 */
function obtenirToutesFichesFrais($idCnx,$mois=null,$idEtat=null) {
    $req = "SELECT * FROM FicheFrais WHERE idVisiteur IN (SELECT idEmploye FROM visiteur)";
    $req.=($mois)?" AND mois = '".filtrerChainePourBD($mois)."'":"";
    $req.=($idEtat)?" AND idEtat = '".filtrerChainePourBD($idEtat)."'":"";
    $req.=" ORDER BY mois" ;
    $idJeuRes = mysql_query($req, $idCnx);
    $lignes = false ;
    if ( $idJeuRes ) {
        while ($ligne = mysql_fetch_assoc($idJeuRes)) 
        {
            $lignes[] = $ligne;
     }
        mysql_free_result($idJeuRes);
    }
    return $lignes ;
}


/** 
 * Fournit le mois de la derni�re fiche de frais d'un visiteur.
 * Retourne le mois de la derni�re fiche de frais du visiteur d'id $unIdVisiteur.
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdVisiteur id visiteur  
 * @return string dernier mois sous la forme AAAAMM
 */
function obtenirDernierMoisSaisi($idCnx, $unIdVisiteur) {
	$requete = "select max(mois) as dernierMois from FicheFrais where idVisiteur='" .
            $unIdVisiteur . "'";
	$idJeuRes = mysql_query($requete, $idCnx);
    $dernierMois = false ;
    if ( $idJeuRes ) {
        $ligne = mysql_fetch_assoc($idJeuRes);
        $dernierMois = $ligne["dernierMois"];
        mysql_free_result($idJeuRes);
    }        
	return $dernierMois;
}

/** 
 * Ajoute une nouvelle fiche de frais et les �l�ments forfaitis�s associ�s, 
 * Ajoute la fiche de frais du mois de $unMois (MMAAAA) du visiteur 
 * $idVisiteur, avec les �l�ments forfaitis�s associ�s dont la quantit� initiale
 * est affect�e � 0. Cl�t �ventuellement la fiche de frais pr�c�dente du visiteur. 
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demand� (MMAAAA)
 * @param string $unIdVisiteur id visiteur  
 * @return void
 */
function ajouterFicheFrais($idCnx, $unMois, $unIdVisiteur) {
    $unMois = filtrerChainePourBD($unMois);
    // modification de la derni�re fiche de frais du visiteur
    $dernierMois = obtenirDernierMoisSaisi($idCnx, $unIdVisiteur);
	$laDerniereFiche = obtenirDetailFicheFrais($idCnx, $dernierMois, $unIdVisiteur);
	if ( is_array($laDerniereFiche) && $laDerniereFiche['idEtat']=='CR'){
		modifierEtatFicheFrais($idCnx, $dernierMois, $unIdVisiteur, 'CL');
	}
    
    // ajout de la fiche de frais � l'�tat Cr��
    $requete = "insert into FicheFrais (idVisiteur, mois, nbJustificatifs, montantValide, idEtat, dateModif) values ('" 
              . $unIdVisiteur 
              . "','" . $unMois . "',0,NULL, 'CR', '" . date("Y-m-d") . "')";
    mysql_query($requete, $idCnx);
    
    // ajout des �l�ments forfaitis�s
    $requete = "select id from FraisForfait";
    $idJeuRes = mysql_query($requete, $idCnx);
    if ( $idJeuRes ) {
        $ligne = mysql_fetch_assoc($idJeuRes);
        while ( is_array($ligne) ) {
            $idFraisForfait = $ligne["id"];
            // insertion d'une ligne frais forfait dans la base
            $requete = "insert into LigneFraisForfait (idVisiteur, mois, idFraisForfait, quantite)
                        values ('" . $unIdVisiteur . "','" . $unMois . "','" . $idFraisForfait . "',0)";
            mysql_query($requete, $idCnx);
            // passage au frais forfait suivant
            $ligne = mysql_fetch_assoc ($idJeuRes);
        }
        mysql_free_result($idJeuRes);       
    }        
}

/**
 * Retourne le texte de la requ�te select concernant les mois pour lesquels un 
 * visiteur a une fiche de frais. 
 * 
 * La requ�te de s�lection fournie permettra d'obtenir les mois (AAAAMM) pour 
 * lesquels le visiteur $unIdVisiteur a une fiche de frais. 
 * @param string $unIdVisiteur id visiteur  
 * @return string texte de la requ�te select
 */                                                 
function obtenirReqMoisFicheFrais($unIdVisiteur,$etat=null) {
    $req = "select fichefrais.mois as mois from  fichefrais where fichefrais.idvisiteur ='"
    . $unIdVisiteur."'";
    $req.=($etat)?' and idEtat =\''.$etat.'\'':'';
    $req.=' order by fichefrais.mois desc';
    return $req ;
}  

/**
 * Retourne le texte de la requ�te select concernant les �l�ments forfaitis�s 
 * d'un visiteur pour un mois donn�s. 
 * 
 * La requ�te de s�lection fournie permettra d'obtenir l'id, le libell� et la
 * quantit� des �l�ments forfaitis�s de la fiche de frais du visiteur
 * d'id $idVisiteur pour le mois $mois    
 * @param string $unMois mois demand� (MMAAAA)
 * @param string $unIdVisiteur id visiteur  
 * @return string texte de la requ�te select
 */                                                 
function obtenirReqEltsForfaitFicheFrais($unMois, $unIdVisiteur) {
    $unMois = filtrerChainePourBD($unMois);
    $requete = "select idFraisForfait, libelle, quantite from LigneFraisForfait
              inner join FraisForfait on FraisForfait.id = LigneFraisForfait.idFraisForfait
              where idVisiteur='" . $unIdVisiteur . "' and mois='" . $unMois . "'";
    return $requete;
}

/**
 * Retourne le texte de la requ�te select concernant les �l�ments hors forfait 
 * d'un visiteur pour un mois donn�s. 
 * 
 * La requ�te de s�lection fournie permettra d'obtenir l'id, la date, le libell� 
 * et le montant des �l�ments hors forfait de la fiche de frais du visiteur
 * d'id $idVisiteur pour le mois $mois    
 * @param string $unMois mois demand� (MMAAAA)
 * @param string $unIdVisiteur id visiteur  
 * @return string texte de la requ�te select
 */                                                 
function obtenirReqEltsHorsForfaitFicheFrais($unMois, $unIdVisiteur) {
    $unMois = filtrerChainePourBD($unMois);
    $requete = "select id, date, libelle, montant,statut from LigneFraisHorsForfait
              where idVisiteur='" . $unIdVisiteur 
              . "' and mois='" . $unMois . "'";
    return $requete;
}

/**
 * Supprime une ligne hors forfait.
 * Supprime dans la BD la ligne hors forfait d'id $unIdLigneHF
 * @param resource $idCnx identifiant de connexion
 * @param string $idLigneHF id de la ligne hors forfait
 * @return void
 */
function supprimerLigneHF($idCnx, $unIdLigneHF) {
    $requete = "delete from LigneFraisHorsForfait where id = " . $unIdLigneHF;
    mysql_query($requete, $idCnx);
}

/**
 * Ajoute une nouvelle ligne hors forfait.
 * Ins�re dans la BD la ligne hors forfait de libell� $unLibelleHF du montant 
 * $unMontantHF ayant eu lieu � la date $uneDateHF pour la fiche de frais du mois
 * $unMois du visiteur d'id $unIdVisiteur
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demand� (AAMMMM)
 * @param string $unIdVisiteur id du visiteur
 * @param string $uneDateHF date du frais hors forfait
 * @param string $unLibelleHF libell� du frais hors forfait 
 * @param double $unMontantHF montant du frais hors forfait
 * @return void
 */
function ajouterLigneHF($idCnx, $unMois, $unIdVisiteur, $uneDateHF, $unLibelleHF, $unMontantHF) {
    $unLibelleHF = filtrerChainePourBD($unLibelleHF);
    $uneDateHF = filtrerChainePourBD(convertirDateFrancaisVersAnglais($uneDateHF));
    $unMois = filtrerChainePourBD($unMois);
    $requete = "insert into LigneFraisHorsForfait(idVisiteur, mois, date, libelle, montant) 
                values ('" . $unIdVisiteur . "','" . $unMois . "','" . $uneDateHF . "','" . $unLibelleHF . "'," . $unMontantHF .")";
    mysql_query($requete, $idCnx);
}

/**
 * Modifie les quantit�s des �l�ments forfaitis�s d'une fiche de frais. 
 * Met � jour les �l�ments forfaitis�s contenus  
 * dans $desEltsForfaits pour le visiteur $unIdVisiteur et
 * le mois $unMois dans la table LigneFraisForfait, apr�s avoir filtr� 
 * (annul� l'effet de certains caract�res consid�r�s comme sp�ciaux par 
 *  MySql) chaque donn�e   
 * @param resource $idCnx identifiant de connexion
 * @param string $unMois mois demand� (MMAAAA) 
 * @param string $unIdVisiteur  id visiteur
 * @param array $desEltsForfait tableau des quantit�s des �l�ments hors forfait
 * avec pour cl�s les identifiants des frais forfaitis�s 
 * @return void  
 */
function modifierEltsForfait($idCnx, $unMois, $unIdVisiteur, $desEltsForfait) {
    $unMois=filtrerChainePourBD($unMois);
    $unIdVisiteur=filtrerChainePourBD($unIdVisiteur);
    foreach ($desEltsForfait as $idFraisForfait => $quantite) {
        $requete = "update LigneFraisForfait set quantite = " . $quantite 
                    . " where idVisiteur = '" . $unIdVisiteur . "' and mois = '"
                    . $unMois . "' and idFraisForfait='" . $idFraisForfait . "'";
      mysql_query($requete, $idCnx);
    }
}

/**
 * Contr�le les informations de connexionn d'un utilisateur.
 * V�rifie si les informations de connexion $unLogin, $unMdp sont ou non valides.
 * Retourne les informations de l'utilisateur sous forme de tableau associatif 
 * dont les cl�s sont les noms des colonnes (id, nom, prenom, login, mdp)
 * si login et mot de passe existent, le bool�en false sinon. 
 * @param resource $idCnx identifiant de connexion
 * @param string $unLogin login 
 * @param string $unMdp mot de passe 
 * @return array tableau associatif ou bool�en false 
 */
function verifierInfosConnexion($idCnx, $unLogin, $unMdp, $unType) {
    $unLogin = filtrerChainePourBD($unLogin);
    $unMdp = sha1($unMdp);
    $typeCompte = filtrerChainePourBD($unType);
    // le mot de passe est crypt� dans la base avec la fonction de hachage sha1
    $sql = "select id, nom, prenom, login, mdp from employe,$typeCompte where login='$unLogin' and mdp='$unMdp' and id in ($typeCompte.idEmploye)";
    $idJeuRes = mysql_query($sql, $idCnx);
    $lignes = false;
    if (mysql_num_rows($idJeuRes) ==1 ) {
        $lignes = mysql_fetch_assoc($idJeuRes);
        $lignes["statut"] = $typeCompte;
        mysql_free_result($idJeuRes);
    }
    return $lignes;
}

/**
 * Modifie l'�tat et la date de modification d'une fiche de frais 

 * Met � jour l'�tat de la fiche de frais du visiteur $unIdVisiteur pour
 * le mois $unMois � la nouvelle valeur $unEtat et passe la date de modif � 
 * la date d'aujourd'hui
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdVisiteur 
 * @param string $unMois mois sous la forme aaaamm
 * @return void 
 */
function modifierEtatFicheFrais($idCnx, $unMois, $unIdVisiteur, $unEtat) {
    $requete = "update FicheFrais set idEtat = '" . $unEtat . 
               "', dateModif = now() where idVisiteur ='" .
               $unIdVisiteur . "' and mois = '". $unMois . "'";
    mysql_query($requete, $idCnx);
}


/**
 * Modifie les informations d'un employ�
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdEmploye
 * @param string[] $fields contenant le ou les champs � modifier
 * @return void 
 */
function modifierInfoEmploye($idCnx,$unIdEmploye,$fields)
{
    $req = "UPDATE employe SET ";
    //on parcourt et on ajoute le champ et sa valeur dans la requete UPDATE
    foreach ($fields as $nom => $valeur) 
    {
        $req.= $nom . " = '" . filtrerChainePourBD($valeur) . "',";
    }
    //on suprime la derni�re virgule
    $req = substr($req,0,-1);
    //la seule condition est l'id de l'utilisateur
    $req.=" WHERE id = "."'".$unIdEmploye."'";
    mysql_query($req, $idCnx);
}

function obtenirEltsHorsForfaitFicheFrais($idCnx, $unMois, $unIdVisiteur) {
    $unMois = filtrerChainePourBD($unMois);
    $req = "select id, date, libelle, montant from LigneFraisHorsForfait
    where idVisiteur='" . $unIdVisiteur 
    . "' and mois='" . $unMois . "'";
    $idJeuEltsHorsForfait = mysql_query($req, $idCnx);
    $lgEltHorsForfait = mysql_fetch_assoc($idJeuEltsHorsForfait);
    while ( is_array($lgEltHorsForfait) ) {
        ?>
        <tr>
            <td><?php echo $lgEltHorsForfait["date"] ; ?></td>
            <td><?php echo filtrerChainePourNavig($lgEltHorsForfait["libelle"]) ; ?></td>
            <td><?php echo $lgEltHorsForfait["montant"] ; ?></td>
        </tr>
        <?php
        $lgEltHorsForfait = mysql_fetch_assoc($idJeuEltsHorsForfait);
    }
    mysql_free_result($idJeuEltsHorsForfait);
    return $lgEltHorsForfait;
}

function obtenirEltsForfaitFicheFrais($idCnx, $unMois, $unIdVisiteur) {
    $unMois = filtrerChainePourBD($unMois);
    $req = "select idFraisForfait, libelle, quantite from LigneFraisForfait
              inner join FraisForfait on FraisForfait.id = LigneFraisForfait.idFraisForfait
              where idVisiteur='" . $unIdVisiteur . "' and mois='" . $unMois . "'";
              $idJeuEltsFraisForfait = mysql_query($req, $idCnx);
              echo mysql_error($idCnx);
              $lgEltForfait = mysql_fetch_assoc($idJeuEltsFraisForfait);
              $tabEltsFraisForfait = array();
              while ( is_array($lgEltForfait) ) {
                $tabEltsFraisForfait[$lgEltForfait["libelle"]] = $lgEltForfait["quantite"];
                $lgEltForfait = mysql_fetch_assoc($idJeuEltsFraisForfait);
              }
              mysql_free_result($idJeuEltsFraisForfait);
    return $lgEltForfait;
}

function obtenirCompteVisiteurs($idCnx)
{
    $req = "SELECT COUNT(idEmploye) as nbVisiteurs FROM visiteur";
    $idJeuRes = mysql_query($req, $idCnx);
    $totalV = mysql_fetch_assoc($idJeuRes);
    echo mysql_error($idCnx);
    mysql_free_result($idJeuRes);
    return $totalV['nbVisiteurs'];
}

function obtenirCompteComptables($idCnx)
{
    $req = "SELECT COUNT(idEmploye) AS nbComptables FROM comptable";
    $idJeuRes = mysql_query($req, $idCnx);
    $totalC = mysql_fetch_assoc($idJeuRes);
    echo mysql_error($idCnx);
    mysql_free_result($idJeuRes);
    return $totalC['nbComptables'];
}

/**
 * Script de cloture des fiches de frais 
 * automatiquement lanc� par une tache chronique
 * manuellement lanc� lors de la visite de la page cValiderFichesFrais.php
 * @param resource $idCnx identifiant de connexion
 * @param jeton $token pour empecher un appel involontaire de la fonction
 * @param le mois $mois qui est la condition de cloture lorsque la fonction appel�e manuellement
 * @return void 
 */
function cloturerFichesFrais($idCnx,$token=null,$mois=null)
{
    if($token=='autorise')
    {
        $req ="UPDATE fichefrais SET idEtat = 'CL'";
        $req.=($mois)?" where mois <='".$mois."'":"";
        mysql_query($req, $idCnx);
    }
    else
    {
        return false;
    }
}


/**
 * Valide la fiche de frais pour l'utilisateur,le mois s�lectionn�
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdVisiteur contient l'id du visiteur
 * @param int $unMois contient le mois � valider
 * @param int $MontantValide contient le montant � valider
 * @param int $nombreJustificatifs contient le nombre de justificatif
 * @return bool
 */
function validationFicheFrais($idCnx,$unIdVisiteur,$unMois,$MontantValide,$nombreJustificatifs)
{
    $req = "update FicheFrais set MontantValide = '" . $MontantValide . 
    "', dateModif = now(), nbJustificatifs ='".$nombreJustificatifs."',idEtat ='VA' where idVisiteur ='" .
    $unIdVisiteur . "' and mois = '". $unMois . "'";
    mysql_query($req, $idCnx);
}

/**
 * Valide les lignes de frais hors forfait 
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdVisiteur contient l'id du visiteur
 * @param string[] $unEtat contient les �tat � modifier
 * @return bool
 */
function validationLignesFraisHorsForfait($idCnx,$unIdVisiteur,$unEtat)
{
    $validation= false;
    //on parcourt et on ajoute le champ et sa valeur dans la requete UPDATE
    //tant qu'il y'a des lignes dans le tableau
        foreach ($unEtat as $idFiche => $etat) 
        {
            $req = "UPDATE lignefraishorsforfait SET ";
            $req.="statut = '" . filtrerChainePourBD($etat) . "' ";
    //on suprime la derni�re virgule
            // $req = substr($req,0,-1);
    //les conditions sont l'id et le mois
            $req.=" WHERE idVisiteur = "."'".$unIdVisiteur."' AND id = ".$idFiche ;
            if(mysql_query($req, $idCnx))
            {
                $validation = true;
            }
        }
    return $validation;
}


/**
 * Passe la fiche de frais de l'utilisateur � l'�tat rembours�
 * @param resource $idCnx identifiant de connexion
 * @param string $unIdVisiteur contient l'id du visiteur
 * @param string $unMois correspond au mois de la fiche de frais
 * @return void
 */
function rembourseFicheFrais($idCnx,$unIdVisiteur,$unMois)
{
    $req ="UPDATE fichefrais SET idEtat = 'RB'";
    $req.=" where mois ='".$unMois."' AND idVisiteur ='".$unIdVisiteur."'";
    return (mysql_query($req, $idCnx))?true:false;
}
?>