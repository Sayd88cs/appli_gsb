
<!-- Affichage étape finale -->
<h3>Fiche de frais du mois de <?php echo obtenirLibelleMois(intval(substr($moisSaisi,4,2))) . " " . substr($moisSaisi,0,4); ?> : 
	<em><?php echo $tabFicheFrais["libelleEtat"]; ?> </em>
	depuis le <em><?php echo $tabFicheFrais["dateModif"]; ?></em></h3>
	<div class="encadre">
		<?php
            // demande de la requête pour obtenir la liste des éléments 
            // forfaitisés du visiteur connecté pour le mois demandé
		$idJeuEltsFraisForfait = mysql_query($req, $idConnexion);
		echo mysql_error($idConnexion);
		$lgEltForfait = mysql_fetch_assoc($idJeuEltsFraisForfait);
            // parcours des frais forfaitisés du visiteur connecté
            // le stockage intermédiaire dans un tableau est nécessaire
            // car chacune des lignes du jeu d'enregistrements doit être
            // affichée au sein d'une colonne du tableau HTML
		$tabEltsFraisForfait = array();
		while ( is_array($lgEltForfait) ) {
			$tabEltsFraisForfait[$lgEltForfait["libelle"]] = $lgEltForfait["quantite"];
			$lgEltForfait = mysql_fetch_assoc($idJeuEltsFraisForfait);
		}
		mysql_free_result($idJeuEltsFraisForfait);
		?>
		<table class="listeLegere">
			<caption>Quantités des éléments forfaitisés</caption>
			<p>Montant validé : <?php echo $tabFicheFrais["montantValide"] ;
			?>
		</p>
		<tr>
			<?php
            // premier parcours du tableau des frais forfaitisés du visiteur connecté
            // pour afficher la ligne des libellés des frais forfaitisés
			foreach ( $tabEltsFraisForfait as $unLibelle => $uneQuantite ) {
				?>
				<th><?php echo $unLibelle ; ?></th>
				<?php
			}
			?>
		</tr>
		<tr>
			<?php
            // second parcours du tableau des frais forfaitisés du visiteur connecté
            // pour afficher la ligne des quantités des frais forfaitisés
			foreach ( $tabEltsFraisForfait as $unLibelle => $uneQuantite ) {
				?>
				<td class="qteForfait"><?php echo $uneQuantite ; ?></td>
				<?php
			}
			?>
		</tr>
	</table>
	<table class="listeLegere">
		<caption>Descriptif des éléments hors forfait - <?php echo $tabFicheFrais["nbJustificatifs"]; ?> justificatifs reçus -
		</caption>
		<tr>
			<th class="date">Date</th>
			<th class="libelle">Libellé</th>
			<th class="montant">Montant</th>
			<th>Statut</th>
		</tr>
		<?php
            // demande de la requête pour obtenir la liste des éléments hors
            // forfait du visiteur séléctionné pour le mois demandé
		$req = obtenirReqEltsHorsForfaitFicheFrais($moisSaisi,$idVisiteur);
		$idJeuEltsHorsForfait = mysql_query($req, $idConnexion);
		$lgEltHorsForfait = mysql_fetch_assoc($idJeuEltsHorsForfait);
		$i=0;
            // parcours des éléments hors forfait 
		while (is_array($lgEltHorsForfait)) {
			?>
			<tr>
				<td><?php echo $lgEltHorsForfait["date"] ; ?></td>
				<td><?php echo filtrerChainePourNavig($lgEltHorsForfait["libelle"]) ; ?></td>
				<td><?php echo $lgEltHorsForfait["montant"] ; ?></td>
				<td id="<?php echo 'etat['.++$i.']' ?>" value="" name="statut"><?php echo $lgEltHorsForfait["statut"] ?></td>
			</tr>
			<?php
			$lgEltHorsForfait = mysql_fetch_assoc($idJeuEltsHorsForfait);
		}
		mysql_free_result($idJeuEltsHorsForfait);
		?>
	</table>
</div>
