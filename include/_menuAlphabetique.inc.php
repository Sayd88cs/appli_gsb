<!-- Division pour le menu[A-Z] -->
<div title="Selectionner lettre pour visiteur">
	<?php
//boucle pour afficher un menu de recherche par lettre de A à Z
	foreach(range('A','Z') as $i) 
	{
		if(!empty($lettre) && $lettre == $i){
			echo "<span style='cursor:default'>$i</span>";
		}
		else
		{
			echo "<a href='?lettre=$i'>$i</a>";
		}
		echo ($i != "Z")? "|":"";
	}
	?>
</div>