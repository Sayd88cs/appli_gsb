<?php
/** 
 * Contient la division pour le sommaire, sujet à des variations suivant la 
 * connexion ou non d'un utilisateur, et dans l'avenir, suivant le type de cet utilisateur 
 * @todo  RAS
 */

?>
<!-- Division pour le sommaire -->
<div id="menuGauche">
 <div id="infosUtil">
  <?php      
  if (estVisiteurConnecte() || estComptableConnecte()) {
    $idUser = obtenirIdUserConnecte() ;
    $lgUser = obtenirDetailEmploye($idConnexion, $idUser);
    $nom = $lgUser['nom'];
    $prenom = $lgUser['prenom'];
    ?>
    <h2>
      <?php  
      echo $nom . " " . $prenom ;
      ?>
    </h2>
    <h3><?php echo ucfirst($_SESSION['statut']); ?> médical</h3>
    <?php
  }
  ?>
</div>
<?php if(estVisiteurConnecte()): ?>
  <ul id="menuList">
   <li class="smenu">
    <a href="cAccueil.php" title="Page d'accueil">Accueil</a>
  </li>
  <li class="smenu">
    <a href="cSeDeconnecter.php" title="Se déconnecter">Se déconnecter</a>
  </li>
    <li class="smenu">
    <a href="cModifierInfos.php" title="Modifier vos informations">Modifier vos informations</a>
  </li>

  <li class="smenu">
    <a href="cSaisieFicheFrais.php" title="Saisie fiche de frais du mois courant">Saisie fiche de frais</a>
  </li>
  <li class="smenu">
    <a href="cConsultFichesFrais.php" title="Consultation de mes fiches de frais">Mes fiches de frais</a>
  </li>
    <li class="smenu">
    <a href="cReclamation.php" title="Effectuer une reclamation">Effectuer une reclamation</a>
  </li>
  <li class="smenu">
    <a href="cSuivreReclamation.php" title="Consultation des reclamations">Consulter mes reclamations</a>
  </li>
</ul>

<?php elseif(estComptableConnecte()): ?>
  <ul id="menuList">
   <li class="smenu">
    <a href="cAccueil.php" title="Page d'accueil">Accueil</a>
  </li>
  <li class="smenu">
    <a href="cSeDeconnecter.php" title="Se déconnecter">Se déconnecter</a>
  </li>
    <li class="smenu">
    <a href="cModifierInfos.php" title="Modifier vos informations">Modifier vos informations</a>
  </li>
    <li class="smenu">
    <a href="cChercherVisiteur.php" title="Rechercher un visiteur">Rechercher un visiteur</a>
  </li>
  <li class="smenu">
    <a href="cValidFichesFrais.php" title="Saisie fiche de frais du mois courant">Valider fiche Frais</a>
  </li>
  <li class="smenu">
    <a href="cSuiviFichesFrais.php" title="Consultation de mes fiches de frais">Suivre fiches de frais</a> 
  </li>
</ul>

<?php endif; ?>
</div>
