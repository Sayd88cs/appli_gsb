<?php
var_dump(get_included_files());
ob_start()?>
<page>
<h3>Fiche de frais du mois de <?php echo obtenirLibelleMois(intval(substr($moisSaisi,4,2))) . " " . substr($moisSaisi,0,4); ?> : 
	<em><?php echo $tabFicheFrais["libelleEtat"]; ?> </em>
	depuis le <em><?php echo $tabFicheFrais["dateModif"]; ?></em></h3>
	<div class="encadre">
		<p>Montant validé : <?php echo $tabFicheFrais["montantValide"] ;
		?>
	</p>
	<?php
	$req = obtenirReqEltsForfaitFicheFrais($moisSaisi, obtenirIdUserConnecte());
	$idJeuEltsFraisForfait = mysql_query($req, $idConnexion);
	echo mysql_error($idConnexion);
	$lgEltForfait = mysql_fetch_assoc($idJeuEltsFraisForfait);
	$tabEltsFraisForfait = array();
	while ( is_array($lgEltForfait) ) {
		$tabEltsFraisForfait[$lgEltForfait["libelle"]] = $lgEltForfait["quantite"];
		$lgEltForfait = mysql_fetch_assoc($idJeuEltsFraisForfait);
	}
	mysql_free_result($idJeuEltsFraisForfait);
	?>
	<table class="listeLegere">
		<p>Quantités des éléments forfaitisés</p>
		<tr>
			<?php
			foreach ( $tabEltsFraisForfait as $unLibelle => $uneQuantite ){
				?>
				<th><?php echo $unLibelle ; ?></th>
				<?php
			}
			?>
		</tr>
		<tr>
			<?php
			foreach ( $tabEltsFraisForfait as $unLibelle => $uneQuantite ){
				?>
				<td class="qteForfait"><?php echo $uneQuantite ; ?></td>
				<?php
			}
			?>
		</tr>
	</table>
	<table class="listeLegere">
		<p>Descriptif des éléments hors forfait - <?php echo $tabFicheFrais["nbJustificatifs"]; ?> justificatifs reçus -</p>
		<tr>
			<th class="date">Date</th>
			<th class="libelle">Libellé</th>
			<th class="montant">Montant</th>
			<th class="statut">Statut</th>
		</tr>
		<?php
		$req = obtenirReqEltsHorsForfaitFicheFrais($moisSaisi, obtenirIdUserConnecte());
		$idJeuEltsHorsForfait = mysql_query($req, $idConnexion);
		$lgEltHorsForfait = mysql_fetch_assoc($idJeuEltsHorsForfait);
		while ( is_array($lgEltHorsForfait) ) {
			?>
			<tr>
				<td><?php echo $lgEltHorsForfait["date"] ; ?></td>
				<td><?php echo filtrerChainePourNavig($lgEltHorsForfait["libelle"]) ; ?></td>
				<td><?php echo $lgEltHorsForfait["montant"] ; ?></td>
				<td><?php echo $lgEltHorsForfait["statut"] ; ?></td>
			</tr>
			<?php
			$lgEltHorsForfait = mysql_fetch_assoc($idJeuEltsHorsForfait);
		}
		mysql_free_result($idJeuEltsHorsForfait);
		?>
	</table>
</div>
</page>
<?php
$content = ob_get_clean();
require($repInclude.'html2pdf/html2pdf.class.php') ;
try {
	$pdf = new HTML2PDF('P', 'A4', 'fr');
	$pdf->pdf->SetDisplayMode('fullpage');
	$pdf->writeHTML($content);
	$pdf->Output('test.pdf');
} catch (HTML2PDF_exeption $e) {
	die($e);
}
?>