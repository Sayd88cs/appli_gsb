<?php 
//Fichier de tache chronique lancé par le serveur débian de manière automatique via une tache cron
//Tâche de cloture des fiches de frais

$repInclude = './include/';
require($repInclude . "_init.inc.php");

//Fonction de test si nous somme le dernier du mois 
function DernierDuMois()
{
	$today = new DateTime('now');
	$tomorrow =  new DateTime('now +1 day');
	$testToday = $today->format('m');
	$testTomorrow = $tomorrow->format('m');
	$bool = $testToday<$testTomorrow;
	unset($testTomorrow);
	unset($testToday);
	return ($bool);
}

if(DernierDuMois())
{
	$token = "autorise";
	cloturerFichesFrais($idConnexion,$token);
}
else
{
	echo 'Ce script s\'execute de façon automatique par une tache chronique du serveur,
	vous n\'avez pas à le lancer directement.';
}

