<?php
/** 
 * Page d'accueil de l'application web AppliFrais
 * @package default
 * @todo  RAS
 */
$repInclude = './include/';
require($repInclude . "_init.inc.php");
  // page inaccessible si visiteur non connecté
if ( !estVisiteurConnecte() && !estComptableConnecte())
{
  header("Location: cSeConnecter.php");
  die();
}

require($repInclude . "_entete.inc.html");
require($repInclude . "_sommaire.inc.php");
$nbVisiteurs = obtenirCompteVisiteurs($idConnexion);
$nbComptables = obtenirCompteComptables($idConnexion);
?>
<!-- Division principale -->
<div id="contenu">
  <h2>Bienvenue sur l'intranet GSB</h2>
  <?php if (estComptableConnecte()): ?>
  <div id="info">
    <p>Bonjour <?php echo $_SESSION['loginUser'] ?>, vous êtes connecté(e) en tant que comptable.</p>
    <p>Reportez vous au sommaire sur la gauche pour utiliser les fonctions de l'application.</p>
    <h3>Options disponibles ?</h3>
    <ul>
      <li>Modifier vos informations</li>
      <li>Chercher un visiteur</li>
      <li>Valider les fiches de frais</li>
      <li>Suivre les fiches de frais</li>
    </ul>
  </div>
<?php elseif(estVisiteurConnecte()): ?>
  <div id="info">
    <p>Bonjour <?php echo $_SESSION['loginUser'] ?>, vous êtes connecté(e) en tant que visiteur.</p>
    <p>Reportez vous au sommaire sur la gauche pour utiliser les fonctions de l'application.</p>
    <h3>Options disponibles ?</h3>
    <ul>
      <li>Modifier vos informations</li>
      <li>Déclarer une fiche de frais</li>
      <li>Consulter vos fiches de frais</li>
    </ul>
  </div>
<?php endif ?>
<hr>
<legend style="font-weight: bold">Combiens d'utilisateurs enregistrés ?</legend>
<p>Nous comptons un total de <?php echo $nbVisiteurs+$nbComptables ?> utilisateur(s) dont <?php echo $nbVisiteurs ?> visiteur(s) et <?php echo $nbComptables ?> comptable(s).</p>

</div>
<?php
require($repInclude . "_pied.inc.html");
require($repInclude . "_fin.inc.php");
?>
