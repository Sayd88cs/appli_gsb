<?php  
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Modifier informations"
 * @package default
 * @todo  RAS
 */
$repInclude = './include/';
require($repInclude . "_init.inc.php");

  // page inaccessible si non authentifié
if (!estComptableConnecte() && !estVisiteurConnecte()) {
	header("Location: cSeConnecter.php");
	die();
}

//configuration des éléments nécéssaires au fonctionnement de la page
$etape = lireDonneePost("etape","demanderModif");
require($repInclude . "_entete.inc.html");
$idUser = obtenirIdUserConnecte();
$donneeUser = obtenirTousDetailEmploye($idConnexion,$idUser);
$champs= array();
$tabSucces = array();

//étape de modification des informations de l'utilisateur incluant la vérification des données saisies
if($etape == "validerModif")
{
	//récupération des champs
	$nom = lireDonneePost("Nom",null);
	$prenom = lireDonneePost("Prénom",null);
	$adresse = lireDonneePost("Adresse",null);
	$ville = lireDonneePost("Ville",null);
	$Code_Postal = lireDonneePost("Code_Postal",null);

	//Validation du nom
	if($nom)
	{
		$nom = (preg_match('/^[aA-zZ-ïîÏÎÂÂâä\s]{3,30}$/',$nom))?$nom:null;
		if((!$nom))
		{
			ajouterErreur($tabErreurs, "nom incorrect : il doit être composé de 3 à 30 lettres.");
		}
		else
		{
			$champs['nom'] = $nom;
			$tabSucces[]= '<p class="info">Le nom a été mis à jour.</p>';
		}
	}

	//validation du prenom
	if($prenom){
		$prenom = (preg_match('/^[aA-zZ-ïîÏÎÂÂâä]{3,30}$/',$prenom))?$prenom:null;
		if((!$prenom))
		{
			ajouterErreur($tabErreurs, "prenom incorrect: il doit être composé de 3 à 30 lettres.");
		}
		else
		{
			$champs['prenom'] = $prenom;
			$tabSucces[]= '<p class="info">Le prénom a été mis à jour.</p>';
		}
	}

	//validation de l'adresse
	if($adresse)
	{
		$adresse = (preg_match('/^[a-zA-Z0-9\s\'"]{3,}$/',$adresse))?$adresse:null;
		if((!$adresse))
		{
			ajouterErreur($tabErreurs, "adresse incorrect: elle doit être composée d'au moins 3 caractères.");
		}
		else
		{
			$champs['adresse'] = $adresse;
			$tabSucces[]= '<p class="info">L\'adresse a été mise à jour.</p>';
		}
	}

	//validation de la ville
	if($ville){
		$ville = (preg_match('/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/',$ville))?$ville:null;
		if((!$ville))
		{
			ajouterErreur($tabErreurs, "ville incorrect: elle doit être composée uniquement de lettres.");
		}
		else
		{
			$champs['ville'] = $ville;
			$tabSucces[]= '<p class="info">La ville a été mis à jour.</p>';
		}
	}

	//validation du code postal
	if($Code_Postal)
	{
		$Code_Postal = (preg_match('/^[0-9]{5}$/',$Code_Postal))?$Code_Postal:null;
		if((!$Code_Postal))
		{
			ajouterErreur($tabErreurs, "code postal incorrect: il doit être composé de 5 numéro.");
		}
		else
		{
			$champs['cp'] = $Code_Postal;
			$tabSucces[]= '<p class="info">Le code postal a été mis à jour.</p>';
		}
	}

	//Si il existe au moins un champ à mettre à jour
	if(!empty($champs))
	{
		modifierInfoEmploye($idConnexion,$idUser,$champs);
		$donneeUser = obtenirTousDetailEmploye($idConnexion,$idUser);
	}
	else
	{
		ajouterErreur($tabErreurs, "Erreur, les données n'ont pas été mises à jour.");
	}
}

//étape de changement du mot de passe
if($etape=="changerPassword")
{
	//récupération des données envoyé par la methode POST
	$mdpActuel=lireDonneePost("mdpActuel",null);
	$mdpNouveau=lireDonneePost("mdpNouveau",null);
	$mdpValide=lireDonneePost("mdpValide",null);
	$lgUser = verifierInfosConnexion($idConnexion, $_SESSION['loginUser'], $mdpActuel, $_SESSION['statut']);

	//verification de l'opération de login avec le mot de passe actuel saisit
	if($lgUser)
	{
		//verification du nouveau mot de passe saisit 2 fois
		if($mdpValide && $mdpNouveau && ($mdpNouveau == $mdpValide))
		{
			//validation du nouveau mot de passe
			$mdpNouveau = (preg_match('/^(?=.*\d)(?=.*[A-Z])[0-9a-zA-Z]{8,}$/',$mdpNouveau))?$mdpNouveau:null;
			if((!$mdpNouveau))
			{
				ajouterErreur($tabErreurs, "faible sécurité du mot de passe : au moins 8 caractères, 1 chiffre et une majuscule.");
			}
			else
			{
				//Ajout du nouveau mot de passe
				$champs['mdp'] = sha1($mdpNouveau);
				modifierInfoEmploye($idConnexion,$idUser,$champs);
				$tabSucces[]= '<p class="info">Le mot de passe a été mis à jour.</p>';
			}
		}
		else
		{
			ajouterErreur($tabErreurs, "Le nouveau mot de passe doit être correctement saisie 2 fois.");
		}
	}
	else
	{
		ajouterErreur($tabErreurs, "Ancient mot de passe incorrect.");
	}
}

//l'inclusion du sommaire se fait en fin de script php pour prendre en compte la modification du nom et du prenom
require($repInclude . "_sommaire.inc.php");
?>
<!-- Division principale -->
<div id="contenu">
	<h2>Modifier vos informations</h2>
	<?php 
	if ($etape == "validerModif" || $etape == "changerPassword") {
		if (nbErreurs($tabErreurs) > 0) {
			echo toStringErreurs($tabErreurs);
		}
		if(!empty($tabSucces))
		{
			foreach ($tabSucces as $messageSucces) 
			{
				echo $messageSucces;
			}
		}
	}  
	?>
	<form action="" method="post">
		<div class="corpsForm">
			<input type="hidden" name="etape" value="validerModif">
			<fieldset>
				<legend>Vos informations</legend>
				<?php foreach ($donneeUser as $type => $donnee): 
				$type = ucfirst(utf8_encode($type));?>
				<p>
					<label id="label_<?php echo $type ?>" for="<?php echo $type ?>"><?php echo $type.' :' ?></label>
					<input  id ="<?php echo $type ?>" type="text" placeholder="<?php echo $donnee ?>" name="<?php echo $type ?>" autocomplete="off">
				</p>
			<?php endforeach; ?>
			<p><span style="font-weight: bold">Indication</span>:<i> Les informations grisées correspondent à celles actuellement enregistrées. </i></p>
		</fieldset>
	</div>
	<div class="piedForm">
		<p>
			<input id="ok" type="submit" value="Valider" size="20" 
			title="Enregistrer les nouvelles valeurs des éléments forfaitisés" />
			<input id="annuler" type="reset" value="Effacer" size="20" />
		</p> 
	</div>
</form>

<div class="corpsForm">
	<form action="" method="post">
		<input type="hidden" name="etape" value="changerPassword">
		<fieldset>
			<legend>Mot de passe</legend>
			<p>
				<label for="mdpActuel">* Mot de passe :</label>
				<input id="mdpActuel" type="password" name="mdpActuel" autocomplete="off" required>
			</p>
			<p>
				<label for="mdpNouveau">* Nouveau mot de passe :</label>
				<input id="mdpNouveau" type="password" name="mdpNouveau" autocomplete="off" required>
			</p>
			<p>
				<label for="mdpValide">* Confirmer nouveau mot de passe :</label>
				<input id="mdpValide" type="password" name="mdpValide" autocomplete="off" required>
			</p>
			<p><span style="font-weight: bold">Indication</span>:<i> le nouveau mot de passe doit contenir au moins 8 caractères, une majuscule,un chiffre.</i></p>
		</fieldset>
	</div>
	<div class="piedForm">
		<p>
			<input id="ok" type="submit" value="Valider" size="20" 
			title="Enregistrer les nouvelles valeurs des éléments forfaitisés" />
			<input id="annuler" type="reset" value="Effacer" size="20" />
		</p> 
	</div>
</form>
</div>

<?php
require($repInclude . "_pied.inc.html");
require($repInclude . "_fin.inc.php");
?>