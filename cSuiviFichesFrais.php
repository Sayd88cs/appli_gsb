<?php  
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Suivi fiche de frais"
 * @package default
 * @todo  RAS
 */
$repInclude = './include/';
require($repInclude . "_init.inc.php");

  // page inaccessible si visiteur non connecté
if (!estComptableConnecte()) {
	header("Location: cSeConnecter.php");
	die();
}

//configuration des éléments nécéssaires au fonctionnement de la page
$etape = lireDonneePost("etape","demanderSuivi");
$titre = "" ;
$rembourse=false;
require($repInclude . "_entete.inc.html");
require($repInclude . "_sommaire.inc.php");


//cette étape récupère les donnés de la fiche de l'utilisateur demandé lors de l'étape précédente
if($etape =="recupererFiche")
{
	$moisSaisi = lireDonneePost("moisSaisi","");
	$nomVisiteur = lireDonneePost("nom","");
	$prenomVisiteur = lireDonneePost("prenom","");
	$idVisiteur = lireDonneePost("idVisiteur","");
	$titre ="Fiche de frais de $nomVisiteur $prenomVisiteur";
	$req = obtenirReqEltsForfaitFicheFrais($moisSaisi,$idVisiteur);
	$tabFicheFrais = obtenirDetailFicheFrais($idConnexion, $moisSaisi, $idVisiteur);
}

//étape finale permetant de défnir l'état de la fiche de frais, ajout de message d'erreur si echec
if ($etape =="rembourserFiche") 
{
	$moisSaisi = lireDonneePost("moisSaisi","");
	$idVisiteur = lireDonneePost("idVisiteur","");
	//on execute la fonction de modification de l'état de la fiche de frais pour définir à "remboursée"
	if(!rembourseFicheFrais($idConnexion,$idVisiteur,$moisSaisi))
	{
		ajouterErreur($tabErreurs, "Erreur, impossible de passer cette fiche frais à l'état de remboursé.");
	}
	else
	{
		$etape ="demanderSuivi";
		$rembourse = true;
	}
}

//étape initiale de la page permétant de récuperer toutes les fiches de frais validés
if($etape =="demanderSuivi")
{
	$fichesFrais = obtenirToutesFichesFrais($idConnexion,null,$idEtat='VA');
	if($fichesFrais)
	{
		$titre = "Voici la liste des fiches de frais en cours de validation et remboursement";
	}
	else
	{
		$titre = "Pas de fiches de frais à rembourser pour le moment";
	}
}

?>
<!-- Division principale -->
<div id="contenu">
	<h2>Suivre le paiement des fiches de frais</h2>
	<?php
	if ( $etape=="rembourserFiche" || $rembourse) 
	{
		if ( nbErreurs($tabErreurs) > 0 ) 
		{
			echo toStringErreurs($tabErreurs);
		}
		else
		{
			echo '<p class="info">La fiche de frais est passé à l\' etat remboursé.</p>';
		}
	}
	?>
	<h3><?php echo $titre ?></h3>
	<?php if($etape == "demanderSuivi" && is_array($fichesFrais)): ?>

	<div class="corpsForm">
		<div>
			<table class="listeLegere" style="table-layout: fixed;width:100%;">
				<?php foreach ($fichesFrais as $uneFiche): ?>
				<tr>
					<th width="20%">Visiteur</th>
					<th width="25%">Justificatif(s)</th>
					<th width="25%">Montant</th>
					<th class="action" width="30%"> Action </th>
				</tr>
				<tr>
					<td>
						<?php $infoVisiteur = obtenirDetailEmploye($idConnexion,$uneFiche['idVisiteur']); ?>
						<?php echo $infoVisiteur['nom'] ?>
						<?php echo $infoVisiteur['prenom'] ?>
					</td>
					<td>
						<?php echo $uneFiche['nbJustificatifs'] ?>
					</td>
					<td>
						<?php echo $uneFiche['montantValide'] ?>
					</td>
					<td>
						<form action="" method="post">
							<input type="hidden" name="etape" value="recupererFiche"/>
							<input type="hidden" name="prenom" value="<?php echo $infoVisiteur['prenom']?>"/>
							<input type="hidden" name="nom" value="<?php echo $infoVisiteur['nom']?>"/>
							<input type="hidden" name="idVisiteur" value="<?php echo $uneFiche['idVisiteur']?>"/>
							<input type="hidden" name="moisSaisi" value="<?php echo $uneFiche['mois']?>"/>
							<input type="submit" style="width:100%" value="Consulter" style=" word-wrap:break-word">
						</form>
					</td>
				</tr>
				&nbsp;
			<?php endforeach ?>
		</table>
	</div>
</div>
<?php endif; ?>
<?php if ($etape=="recupererFiche"): ?>
	<?php require($repInclude."_tableauFicheFrais.inc.php"); ?>
	<div class="piedForm">
		<p>
			<form action="" method="post">
				<input type="hidden" name="etape" value="rembourserFiche"/>
				<input type="hidden" name="idVisiteur" value="<?php echo $idVisiteur?>"/>
				<input type="hidden" name="moisSaisi" value="<?php echo $moisSaisi?>"/>
				<input type="submit" value="Defnir comme remboursé" size="20" 
				title="Retourner à la page précédente" />
				<input type="button" value="Retour" size="20" 
				title="Retourner à la page précédente" onclick="history.back();" />
			</form>
		</p> 
	</div>
<?php endif ?>
</div>
<?php
require($repInclude . "_pied.inc.html");
require($repInclude . "_fin.inc.php");
?>

<?php 
/*
1.	L’utilisateur demande à suivre le paiement les fiches de frais.
2.	Le système propose de choisir une fiche de frais parmi celles à valider et mises en paiement
3.	L’utilisateur sélectionne les informations et valide
4.	Le système affiche le détail de la fiche de frais –frais forfaitisés et hors forfait
5.	L’utilisateur « Met en paiement » la fiche de frais
6.	Le système modifie l’état de la fiche à « Mise en paiement » et met à jour la date de modification.

 */ ?>