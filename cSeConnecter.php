<?php  
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Se connecter"
 * @package default
 * @todo  RAS
 */

$repInclude = './include/';
require($repInclude . "_init.inc.php");

  // page inaccessible si visiteur ou comptable connecté
if ( estVisiteurConnecte() || estComptableConnecte())
{
  header("Location: cAccueil.php");
  die();
}

  // est-on au 1er appel du programme ou non ?
$etape=(count($_POST)!=0)?'validerConnexion' : 'demanderConnexion';

  if ($etape=='validerConnexion') { // un client demande à s'authentifier
      // acquisition des données envoyées, ici login et mot de passe
  $login = lireDonneePost("txtLogin");
  $mdp = lireDonneePost("txtMdp");
  $type = lireDonneePost("txtType");
  $lgUser = verifierInfosConnexion($idConnexion, $login, $mdp, $type);
      // si l'id utilisateur a été trouvé, donc informations fournies sous forme de tableau
  if ( is_array($lgUser) ) { 
    affecterInfosConnecte($lgUser["id"], $lgUser["login"], $lgUser["statut"]);
  }
  else {
    ajouterErreur($tabErreurs, "Pseudo et/ou mot de passe incorrects");
  }
}
if ( $etape == "validerConnexion" && nbErreurs($tabErreurs) == 0 ) {
  header("Location:cAccueil.php");
  die();
}

require($repInclude . "_entete.inc.html");
require($repInclude . "_sommaire.inc.php");

?>
<!-- Division pour le contenu principal -->
<div id="contenu">
  <h2>Identification utilisateur</h2>
  <?php
  if ( $etape == "validerConnexion" ) 
  {
    if ( nbErreurs($tabErreurs) > 0 ) 
    {
      echo toStringErreurs($tabErreurs);
    }
  }
  ?>               
  <form id="frmConnexion" action="" method="post">
    <div class="corpsForm" style="zoom:1.4; ">
      <input type="hidden" name="etape" id="etape" value="validerConnexion" />
      <p>
        <label for="txtLogin" accesskey="n">* Login : </label>
        <input type="text" id="txtLogin" name="txtLogin" maxlength="20" size="15" value="" title="Entrez votre login" required/>
      </p>
      <p>
        <label for="txtMdp" accesskey="m">* Mot de passe : </label>
        <input type="password" id="txtMdp" name="txtMdp" size="15" value=""  title="Entrez votre mot de passe" required/>
      </p>
      <div style="text-align: center;">
        <label for="Comptable" accesskey="c" style ="float:none;"> Comptable </label>
        <input type="radio" name="txtType" id="Comptable" value="comptable">


        <label for="Visiteur" accesskey="v" style ="float:none;"> Visiteur </label>
        <input type="radio" name="txtType" id="Visiteur" value="visiteur" checked="checked">
      </div>

    </div>
    <div class="piedForm">
      <p>
        <input type="submit" id="ok" value="Valider" />
        <input type="reset" id="annuler" value="Effacer" />
      </p> 
    </div>
  </form>
</div>
<?php
require($repInclude . "_pied.inc.html");
require($repInclude . "_fin.inc.php");
?>