<?php
/** 
* Script de contrôle et d'affichage du cas d'utilisation "Valider fiches frais"
* @package default
* @todo  RAS
*/
$repInclude = './include/';
require($repInclude . "_init.inc.php");

// page inaccessible si comptable non connecté
if ( !estComptableConnecte() ) {
  header("Location: cSeConnecter.php");
  die();
}

//configuration des éléments nécéssaires au fonctionnement de la page
$etape = lireDonneePost("etape","debutCampagne");
$titre = "";
require($repInclude . "_entete.inc.html");
//tableau d'affichage des messages de réussite d'une tache
$tabSucces = array();
require($repInclude . "_sommaire.inc.php");
$lesVisiteurs = obtenirTousLesVisiteurs($idConnexion);
//utilisation d'un intervalle pour les mois
$fin = new DateTime();
$debut = new DateTime();
$debut = $debut->modify( '-1 year' ); 
$interval = new DateInterval('P1M');
$daterange = new DatePeriod($debut, $interval ,$fin);



// structure de décision sur les différentes étapes du cas d'utilisation
// Si nous somme entre le 10 et le 20 du mois alors fiches de frais du mois précédent cloturées

// if($etape =="debutCampagne")
// {
if (date('d') >= 10 && date('d') <= 20) 
{
  $moisSaisi = sprintf("%04d%02d", date("Y"), date("m")-1);
  cloturerFichesFrais($idConnexion,'autorise',$moisSaisi);
}
else
{
  //$etape="debutCampagne";
  ajouterErreur($tabErreurs, "Validation des fiches de frais uniquement possible entre le 10 et le 20 de chaques mois");
  ajouterErreur($tabErreurs, "(décomenter \$etape avant mise en prod)");
}
// }

//étape d'affichage de la fiche selectionné si toutefois celle-ci existe
if ($etape == "afficherFiche") 
{
  $idVisiteur = lireDonneePost("visiteur","");
  $moisSaisi = lireDonneePost("mois","");
  $infoVisiteur = obtenirDetailEmploye($idConnexion,$idVisiteur);
  //verification de l'existence de la fiche frais pour le visiteur et pour le mois choisi
  if (existeFicheFrais($idConnexion,$moisSaisi,$idVisiteur,'CL')) 
  {
    $titre ="Visiteur : $infoVisiteur[nom] $infoVisiteur[prenom]";
    $req = obtenirReqEltsForfaitFicheFrais($moisSaisi,$idVisiteur);
    $tabFicheFrais = obtenirDetailFicheFrais($idConnexion, $moisSaisi, $idVisiteur,"CL");
  }
  else
  {
    $etape="debutCampagne";
    $moisSaisi = substr($moisSaisi,4,2);
    $moisSaisi = htmlentities(strftime("%B", mktime(0, 0, 0, $moisSaisi,10)));
    ajouterErreur($tabErreurs, "Pour $moisSaisi, il n'y'a aucune fiche à valider concernant $infoVisiteur[prenom] $infoVisiteur[nom]");
  }

//edition de la fiche de frais
} 
if ($etape == "editionFicheFrais")
{
  $titre="Modification de la fiche de frais";
  $idVisiteur = lireDonneePost("idVisiteur","");
  $moisSaisi = lireDonneePost("moisSaisi","");
  $tabFicheFrais = obtenirDetailFicheFrais($idConnexion, $moisSaisi, $idVisiteur,"CL");
  $req = obtenirReqEltsHorsForfaitFicheFrais($moisSaisi,$idVisiteur);
  $idJeuEltsHorsForfait = mysql_query($req, $idConnexion);
  $lgEltHorsForfait = mysql_fetch_assoc($idJeuEltsHorsForfait);

  if($tabFicheFrais)
  {
    $infoVisiteur = obtenirDetailEmploye($idConnexion,$idVisiteur);
    $req = obtenirReqEltsForfaitFicheFrais($moisSaisi,$idVisiteur);
    $libelleMois = substr($moisSaisi,4,2);
    $libelleMois = strftime("%B %Y", mktime(0, 0, 0, $libelleMois,10));
  }
  else
  {
    $etape="afficherFiche";
  }
}

if ($etape=="validationFiche") 
{
   $nbJustificatifs = lireDonneePost("NbJustificatifs","0");
   $statut = lireDonneePost("statut","");
   $aucunFraisHorsForfait = lireDonneePost("aucunFraisHorsForfait","");
   $MontantValide = lireDonneePost("MontantValide","0");
   $idVisiteur = lireDonneePost("idVisiteur","");
   $moisSaisi = lireDonneePost("moisSaisi","");

   if($MontantValide && $nbJustificatifs)
   {
    validationFicheFrais($idConnexion,$idVisiteur,$moisSaisi,$MontantValide,$nbJustificatifs);
    $tabSucces[] = '<p class="info">La fiche de frais a été mis à jour.</p>';
   }
   else
   {
     ajouterErreur($tabErreurs, "Erreur la fiche de frais n'a pas été mise à jour, les champs sont vides");
   }

   if($statut)
   {
    validationLignesFraisHorsForfait($idConnexion,$idVisiteur,$statut);
    $tabSucces[] = '<p class="info">Les éléments hors forfait ont été mis à jour.</p>';
   }
   else
   {
    if(!$aucunFraisHorsForfait)
    {
     ajouterErreur($tabErreurs, "Erreur les éléments hors forfait n'ont pas été mis à jour");
    }
   }
   $etape="debutCampagne";
}
?>

<!-- Division principale -->
<div id="contenu">
  <h2>Valider fiches de frais</h2>
  <?php
  if ( nbErreurs($tabErreurs) > 0 ) 
  {
    echo toStringErreurs($tabErreurs);
  }
  if(!empty($tabSucces))
  {
    foreach ($tabSucces as $messageSucces) 
    {
      echo $messageSucces;
    }
  }
  ?>
  <?php echo($titre)?"<h3>$titre</h3>":""; ?>

  <?php if ($etape=="debutCampagne"): ?>
  <form action="" method="post">
    <div class="corpsForm">
      <input type="hidden" name="etape" value="validerModif">
      <fieldset>
        <legend>Fiche à valider :</legend>
        <p>
          <label for="mois">Mois : </label>
          <select id="mois" name="mois" title="Sélectionnez le mois"> 
            <?php foreach($daterange as $date):
            $libelleMois = utf8_encode(strftime("%B %y", $date->getTimestamp()));
            $moisSaisi = $date->format("Ym"); ?>
            <option value="<?php echo $moisSaisi ?>"><?php echo $libelleMois ?></option>
          <?php endforeach ?>
        </select>
      </p>
      <p>
        <label for="visiteur">Visiteur : </label>
        <select id="visiteur" name="visiteur" title="Sélectionnez le visiteur"> 
          <?php foreach ($lesVisiteurs as $visiteur): ?>
          <option value="<?php echo $visiteur['id'] ?>"><?php echo $visiteur['nomPrenom'] ?></option>
        <?php endforeach ?>
      </select>
    </p>
  </fieldset>
</div>
<div class="piedForm">
  <p>
    <input type="hidden" name="etape" value="afficherFiche">
    <input id="ok" type="submit" value="Valider" size="20" 
    title="Chercher fiche de frais en attente de validation" />
    <input id="annuler" type="reset" value="Effacer" size="20" />
  </p> 
</div>
</form>
<?php endif ?>
<?php if ($etape =="afficherFiche"): ?>
  <?php require($repInclude."_tableauFicheFrais.inc.php"); ?>
  <div class="piedForm">
    <p>
      <form action="" method="post">
        <input type="hidden" name="etape" value="editionFicheFrais">
        <input type="hidden" name="idVisiteur" value ="<?php echo $idVisiteur ?>">
        <input type="hidden" name="moisSaisi" value ="<?php echo $moisSaisi ?>">
        <input type="button" value="retour" onClick="history.back();">
        <input type="submit" value="Mettre à jour" size="20" 
        title="Sauvegarde les modifications effectués" 
        onClick="return(confirm('Modifier la fiche de frais?'));">
      </form>
    </p> 
  </div>
<?php endif ?>

<?php if ($etape=="editionFicheFrais"): ?>
  <form action="" method="post">
    <div class="corpsForm">
      <input type="hidden" name="etape" value="validerModif">
      <fieldset>
        <legend>Fiche de frais de <?php echo $infoVisiteur['nom'].' '.$infoVisiteur['prenom'].', '.$libelleMois?></legend>
        <p>
          <label id="label_NbJustificatifs" for="NbJustificatifs">Nombre de justificatif :</label>
          <input  id ="NbJustificatifs" type="text" placeholder="<?php echo $tabFicheFrais['nbJustificatifs'] ?>" name="NbJustificatifs" autocomplete="off">
        </p>
        <p>
          <label id="MontantValide" for="MontantValide">Montant valide (en EUR) :</label>
          <input  id ="MontantValide" type="text" placeholder="<?php echo $tabFicheFrais['montantValide'] ?>" name="MontantValide" autocomplete="off">
        </p>
        <p><span style="font-weight: bold">Indication</span>:<i> Le montant et le nombre de justificatif ne peuvent être validé qu'une seule fois. </i></p>
        <hr/><br/>
        <p style="font-weight: bold;text-decoration: underline">Frais hors forfait :</p>
        <?php if(is_array($lgEltHorsForfait)): ?>
        <table class="listeLegere">
          <tr>
            <th class="date">Date</th>
            <th class="libelle">Libellé</th>
            <th class="montant">Montant</th>
            <th>Statut</th>
          </tr>
          <?php
          $i=0;
          while (is_array($lgEltHorsForfait)):?>
          <tr>
            <td><?php echo $lgEltHorsForfait["date"] ; ?></td>
            <td><?php echo $lgEltHorsForfait["libelle"] ; ?></td>
            <td><?php echo $lgEltHorsForfait["montant"] ; ?></td>
            <td>
              <select id="statut" name="statut[<?php echo $lgEltHorsForfait["id"] ?>]" title="Sélectionnez le statut">
                <option value="valide">Validé</option>
                <option value="refuse">Refusé</option>
              </select>
            </td>
          </tr>
          <?php
          $lgEltHorsForfait = mysql_fetch_assoc($idJeuEltsHorsForfait);
          endwhile; ?>
          <?php mysql_free_result($idJeuEltsHorsForfait); ?> 
        </table>
      <?php else: ?>
      <input type="hidden" name="aucunFraisHorsForfait" value="true">
              <p>Pas de frais hors forfait</p>
      <?php endif ?>
      </fieldset>
    </div>
    <div class="piedForm">
      <p>
        <input type="hidden" name="etape" value="validationFiche">
        <input type="hidden" name="idVisiteur" value ="<?php echo $idVisiteur ?>">
        <input type="hidden" name="moisSaisi" value ="<?php echo $moisSaisi ?>">
        <input id="ok" type="submit" value="Valider" size="20" title="Enregistrer les nouvelles valeurs des éléments forfaitisés" />
        <input id="annuler" type="reset" value="Effacer" size="20" />
      </p> 
    </div>
  </form>
<?php endif ?>
</div>
<?php
require($repInclude . "_pied.inc.html");
require($repInclude . "_fin.inc.php");
?>